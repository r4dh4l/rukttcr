# ruKTTCR - r4dh4l‘s ultimate KILL TEAM TOURNAMENT COMMAND ROSTER

A [tabletop](https://en.wikipedia.org/wiki/Miniature_wargaming) project for a better "[Warhammer 40k](https://en.wikipedia.org/wiki/Warhammer_40,000): Kill Team" Command Roster.

![rukttcr_example2019-12-31_13-45-54.png](media/rukttcr_example2019-12-31_13-45-54.png "rukttcr example picture")

The idea of this Command Roster is to make a tournament preparation more comfortable by:
- combinining the classical Command Roster and Data Cards concept of Games Workshop into one single sheet of paper while all information are visible if printed in landscape format 2 pages on 1 to save table space during matches
- instead of drawing data cards from your Command Roster you just tick the units you want to participate in the current match (assuming a Tournament with 4 matches)
- providing fields to log your tournament progress
- providing fields to say "this unit has exactly the same profile values as unit X" to save time filling out the Command Roster by hand

How to use the files:

1. `rukttcr.odt`: This is the digital fillable **O**pen**D**ocument**T**ext formated file created with [LibreOffice](https://www.libreoffice.org/) (you can use any other office program which can handle ODT to edit the file but please share derivates of the file as ODT as well so that a freedom respecting handling of the file is ongoing possible). `rukttcr.odt` can be used
- to fill out the Command Roster digitally before printing it directly out of the ODT file
- to adjust the file before printing it to fill it out by hand
- to export the file as PDF (in case you want to use it on a device which can only handle PDF format)

2. `rukttcr_2on1landscape.pdf`: This is a PDF print from `rukttcr.odt` as 2 pages on 1 in landscape mode for the original use case described at the beginning. Unfortunately it is not digitally fillable currently but I will try to implement digital input fields in a next version. The only use case for this I imagined is to provide a printing template for people who don't know how to open `rukttcr.odt`.

3. `rukttcr.pdf`: The same like `rukttcr_2on1landscape.pdf` but not as 2 on 1 landscape print, so exactly the same as `rukttcr.odt` in "look and feel". I thought some people would have problems with the small letters of `rukttcr_2on1landscape.pdf` so maybe this version is better readable (while still making it possible to have everything on one sheet of paper by printing it double-sided).


